<?php 
require_once "conexion.php";

$axiosPOST = json_decode(file_get_contents('php://input'), true);

$stmt = Conexion::conectar()->prepare("CALL save_results(?,?,?)");
$stmt->bindParam(1, $axiosPOST['cpu'], PDO::PARAM_STR);
$stmt->bindParam(2, $axiosPOST['ply'], PDO::PARAM_STR);
$stmt->bindParam(3, $axiosPOST['win'], PDO::PARAM_INT);
$stmt->execute();

exit;
