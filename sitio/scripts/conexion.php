<?php

class Conexion {
	static public function conectar(){
		// PDO("nombre del servidor; nombre de la DB","usuario",contaseña);
		$link = new PDO("mysql:host=".getenv("SQL_SERVER")."; dbname=".getenv("JKP_DATABASE"), getenv("JKP_USER"), getenv("JKP_PASSWORD"));
		$link->exec("set names utf8");
		return $link;
	}
}
