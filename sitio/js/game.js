Vue.config.devtools = true;
const vm = new Vue({
	el: "#jankenpon",
	mounted() { 
      axios.get(this.scriptMoves)
         .then(respuesta => this.moves = respuesta.data)
            .catch(error => console.error(error));
      axios.get(this.scriptNofGames)
         .then(respuesta => this.nOfGames = respuesta.data)
            .catch(error => console.error(error));
      axios.get(this.scriptTotalGames)
         .then(respuesta => this.totalGames = respuesta.data)
            .catch(error => console.error(error));
      axios.get(this.scriptTopThreeCpu)
         .then(respuesta => this.topThreeCpu = respuesta.data)
            .catch(error => console.error(error));
      axios.get(this.scriptTopThreePly)
         .then(respuesta => this.topThreePly = respuesta.data)
            .catch(error => console.error(error));
    },
	data: {		
		count: 0,
		countGame: 0,
		countGameWinCPU: 0,
		countGameWinPlayer: 0,
		countGameTie: 0,
		countWinCPU: 0,
		countWinPlayer: 0,
		countTie: 0,
		name: "",
		player: "",
		playerMove: "",
		cpuMove: "",		
		gamePly: "",
		gameCPU: "",
		gameResult: "",
		totalGames: "",
		moves: [],
		nOfGames: [],
		topThreeCpu: [],
		topThreePly: [],
		topCpu: [],
		scriptMoves: "./scripts/moves.php",
		scriptSave: "./scripts/saveResults.php",
		scriptNofGames: "./scripts/nOfGames.php",
		scriptTotalGames: "./scripts/totalGames.php",
		scriptTopThreeCpu: "./scripts/topThreeCpu.php",
		scriptTopThreePly: "./scripts/topThreePly.php"
	},
	watch: {
		count() {
			if (this.count == 3) {
				if (this.countWinPlayer > this.countWinCPU) {
					this.countGameWinPlayer ++;
					this.gameResult = 1;
				}
				if (this.countWinPlayer < this.countWinCPU) {
					this.countGameWinCPU ++;
					this.gameResult = 0;
				}
				if (this.countWinPlayer == this.countWinCPU) {
					this.countGameTie ++;
					this.gameResult = 2;
				}						
			}	
		}
	},
	computed: {
		gameMoves(){
			return 
		}
	},
	methods: {
		playerName() {
			if (this.name.length <= 8) {
				this.player = this.name;
				this.name = null;	
			} else {
				alert("El nombre no puede exeder los 8 caracteres");
			}					
		},
		playGame(movePly,moveIcon,movePrefix) {
			if (this.count <= 2) {
				moveCpu = Math.floor(Math.random() * 3) + 1;
				this.playerMove = moveIcon;
				this.gamePly = this.gamePly + movePrefix;
				
				for (var i = 0; i < 3; i ++) {
					if (this.moves[i].id == moveCpu) {
						this.cpuMove = this.moves[i].icon;
						this.gameCPU = this.gameCPU + this.moves[i].prefix;
					}
				}
				if (movePly == 1 && moveCpu == 1) {
					this.countTie ++;
					this.count ++;	
				} 
				if (movePly == 2 && moveCpu == 2) {
					this.countTie ++;
					this.count ++;	
				} 
				if (movePly == 3 && moveCpu == 3) {
					this.countTie ++;
					this.count ++;	
				} 
				if (movePly == 1 && moveCpu == 2) {
					this.countWinCPU ++;
					this.count ++;	
				} 
				if (movePly == 2 && moveCpu == 3) {
					this.countWinCPU ++;
					this.count ++;	
				} 
				if (movePly == 3 && moveCpu == 1) {
					this.countWinCPU ++;
					this.count ++;	
				} 
				if (movePly == 1 && moveCpu == 3) {
					this.countWinPlayer ++;
					this.count ++;	
				} 					
				if (movePly == 2 && moveCpu == 1) {
					this.countWinPlayer ++;
					this.count ++;	
				} 					
				if (movePly == 3 && moveCpu == 2) {
					this.countWinPlayer ++;
					this.count ++;	
				}				
			} 
		},
		reset() {
			axios.post(this.scriptSave, {// un script de php se encarga de grabar en la DB los datos
                cpu: this.gameCPU,
                ply: this.gamePly,
                win: this.gameResult,                
            })
            .then(respuesta => { 
               console.log(respuesta);                
            })
            .catch(error => console.error(error));
			this.count = 0;
			this.countWinCPU = 0;
			this.countWinPlayer = 0;
			this.countTie = 0;
			this.playerMove = "";
			this.cpuMove = "";
			this.gamePly = "";
			this.gameCPU = "";
			this.gameResult = "";
		}
	}
});