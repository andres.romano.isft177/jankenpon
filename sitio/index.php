<!DOCTYPE html>
<html>
<head>		
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="./css/style.css">
	<link rel="stylesheet" type="text/css" href="./css/bootstrap-4.6.1.min.css">
	<script src="./js/c5b742650e.js"></script>
	<title>Jan Ken Pon</title>
</head>
<body>
	<script src="./js/axios.min.js"></script>
	<script src="./js/vue.min.js"></script>
	<main class="container-md mb-3" id="jankenpon">
		<!-- Encabezado -->
		<div class="row mt-3 justify-content-center">
			<div class="col-12">
				<h3 class="text-center text-uppercase py-3 mb-3">jugar piedra papel o tijera</h3>
				<div v-show="player === '' " class="text-center mt-5">
					<form @submit.prevent="playerName">
						<h4>¡Ingresá tu nombre para jugar!</h4>
						<input type="text" v-model="name" placeholder="Ingresa tu nombre">
						<input type="submit" value="OK">
					</form>			
				</div>
			</div>			
		</div>		
		<!-- Reglamento y resultados -->
		<div class="row mt-3 justify-content-center" v-if="player !== ''">
			<div class="col-md-4 col-sm-12 col-12">
				<h4 class="text-center">Reglamento</h4>
				<ul>¿Quién gana a quién? 
					<li>La tijera gana al papel porque le puede cortar. </li>
					<li>La piedra gana a las tijeras porque las rompe.  </li>
					<li>El papel gana a la piedra porque la envuelve. </li>
					<li>El mejor de 3 gana la partida</li>
				</ul>
			</div>
			<div class="col-md-4 col-sm-12 col-12">
				<h4 class="text-center">Partidas Jugadas</h4>
				<table class="table table-bordered text-center">					
					<tbody>						
						<tr>
							<th>Jugador</th>
							<th>Cantidad</th>
						</tr>
						<tr>
							<td>CPU</td>
							<td>{{ countGameWinCPU }}</td>							
						</tr>
						<tr>
							<td>{{ player }}</td>
							<td>{{ countGameWinPlayer }}</td>
						</tr>
						<tr>
							<td>Empates</td>
							<td>{{ countGameTie }}</td>
						</tr>
						<tr>
							<td><b>Total</b></td>
							<td>{{ countGameTie + countGameWinPlayer + countGameWinCPU }}</td>
						</tr>
					</tbody>					
				</table>
			</div>
			<div class="col-md-4 col-sm-12 col-12">
				<h4 class="text-center">Resultados</h4>
				<table class="table table-bordered text-center">					
					<tbody>						
						<tr class="text-uppercase">
							<th>CPU</th>
							<th>{{ player }}</th>
							<th>EMPATE</th>
						</tr>
						<tr>
							<td>{{ countWinCPU }}</td>
							<td>{{ countWinPlayer }}</td>
							<td>{{ countTie }}</td>
						</tr>
					</tbody>					
				</table>
			</div>			
		</div>		
		<!-- Juego -->
		<div class="row mt-3 justify-content-center" v-if="player !== ''">
			<div class="col-md-8 col-sm-12 col-12">
				<div class="game">
					<div class="d-flex">						
						<div v-show="count != 0" class="card">
							<h4 class="text-center">CPU</h4>
							<div class="move">
								<i :class="cpuMove"></i>
							</div>
						</div>	
						<div v-show="count != 0" class="card">
							<h4 class="text-center">{{ player }}</h4>
							<div class="move">
								<i :class="playerMove"></i>
							</div>
						</div>
					</div>					
				</div>
				<div class="m-auto py-3">
					<button v-show="count == 3" @click.self="reset()" class="btn btn-secondary btn-block">¿Jugar de nuevo?</button>
				</div>			
				<div class="text-center">
					<button v-for="move in moves" v-show="count != 3" @click.self="playGame(move.id,move.icon,move.prefix)" class="btn btn-primary mr-1 ml-1"><i :class="move.icon"></i>{{ move.name }}</button>	
				</div>
			</div>
		</div>		
		<!-- Estadisticas -->
		<div class="row mt-3" v-if="player !== '' && totalGames[0].total > 20">
			<div class="col-12">
				<h2 class="text-center">Estadisticas Generales</h2>
				<table class="table table-bordered text-center">
					<thead>
						<tr>
							<th colspan="3">Resultados Generales de partidas ganadas</th>
						</tr>						
					</thead>
					<tbody>
						<tr>
							<td>CPU</td>
							<td>Jugadores</td>
							<td>Empates</td>
						</tr>					
						<tr>
							<td class="display-4">{{ nOfGames[0].winner }}</td>
							<td class="display-4">{{ nOfGames[1].winner }}</td>
							<td class="display-4">{{ nOfGames[2].winner }}</td>
						</tr>						
					</tbody>
					<tfoot>
						<tr>
							<th colspan="3">Total de partidas jugadas</th>
						</tr>
						<tr>
							<td colspan="3" class="display-4">{{ totalGames[0].total }}</td>
						</tr>
					</tfoot>
				</table>					
			</div>
		</div>
		<!-- Estadisticas individuales-->
		<div class="row mt-3" v-if="player !== '' && totalGames[0].total > 20">
			<div class="col-12">
				<h2 class="text-center">Estadisticas de Jugadas</h2>
				<table class="table table-bordered text-center">
					<thead>
						<tr>
							<th colspan="3">Resultados de partidas</th>
						</tr>						
					</thead>
					<tbody>						
						<tr>
							<th colspan="3">Top 3 jugadas ganadoras de CPU</th>
						</tr>
						<tr>
							<th colspan="1">Cantidad de veces</th>
							<th colspan="2">Jugada ganadora</th>
						</tr>
						
						<tr v-for="handsCpu in topThreeCpu" >						
							<td class="display-4">{{ handsCpu.best_game }}</td>
							<td class="display-4">
								<i :class="handsCpu.mano_1_icon"></i>
								<i :class="handsCpu.mano_2_icon"></i>
								<i :class="handsCpu.mano_3_icon"></i>
							</td>
						</tr>
						<tr>
							<th colspan="3">Top 3 jugadas ganadoras de los Jugadores</th>
						</tr>
						<tr>
							<th colspan="1">Cantidad de veces</th>
							<th colspan="2">Jugada ganadora</th>
						</tr>						
						<tr v-for="handsPly in topThreePly" >						
							<td class="display-4">{{ handsPly.best_game }}</td>
							<td class="display-4">
								<i :class="handsPly.mano_1_icon"></i>
								<i :class="handsPly.mano_2_icon"></i>
								<i :class="handsPly.mano_3_icon"></i>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!-- La más elegida -->
		<div class="row mt-3" v-if="player !== '' && totalGames[0].total > 20">
			<div class="col-12">
				<h2 class="text-center">¡La más elegida!</h2>
				<table class="table table-bordered text-center">
					<thead>
						<tr>
							<th class="display-4"><i :class="moves[0].icon"></i></th>
							<th class="display-4"><i :class="moves[1].icon"></i></th>
							<th class="display-4"><i :class="moves[2].icon"></i></th>
						</tr>
					</thead>
					<tbody>												
						<tr>
							<td class="display-4">{{ moves[0].count }}</td>
							<td class="display-4">{{ moves[1].count }}</td>
							<td class="display-4">{{ moves[2].count }}</td>
						</tr>						
					</tbody>
				</table>
			</div>
		</div>
		<div class="row mt-3" v-if="player !== '' && totalGames[0].total < 20">
			<h1 class="text-center text-uppercase">El juego recolecta datos y generará estadísticas cuando haya un mínimo de 20 partidas jugadas</h1>
		</div>
	</main>
	<script src="./js/game.js"></script>
	<script src="./js/jquery-3.5.1.slim.min.js"></script>
	<script src="./js/popper-1.16.1.min.js"></script>
	<script src="./js/bootstrap-4.6.1.min.js"></script>
</body>
</html>