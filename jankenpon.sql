-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: mysqldb
-- Tiempo de generación: 22-10-2024 a las 23:49:19
-- Versión del servidor: 5.7.44
-- Versión de PHP: 8.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `jankenpon`
--
CREATE DATABASE IF NOT EXISTS `jankenpon` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `jankenpon`;

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`jankenpon`@`%` PROCEDURE `moves` ()  READS SQL DATA COMMENT 'datos para las jugadas' BEGIN
	SELECT 
    	hands.hand_id AS "id",
        hands.hand_name AS "name",
        hands.hand_prefix AS "prefix",
        hands.hand_icon AS "icon",
        hands.hand_cont AS "count"
    FROM hands;
END$$

CREATE DEFINER=`jankenpon`@`%` PROCEDURE `save_results` (IN `cpu` VARCHAR(5) CHARSET utf8mb4, IN `ply` VARCHAR(5) CHARSET utf8mb4, IN `win` TINYINT)   BEGIN
	DECLARE id_game_cpu TINYINT DEFAULT 0;
    DECLARE id_game_ply TINYINT DEFAULT 0;
    DECLARE count_j TINYINT DEFAULT 0;
    DECLARE count_k TINYINT DEFAULT 0;
    DECLARE count_p TINYINT DEFAULT 0;
    DECLARE cpu_mano_uno VARCHAR(1);
    DECLARE ply_mano_uno VARCHAR(1);
    DECLARE cpu_mano_dos VARCHAR(1);
    DECLARE ply_mano_dos VARCHAR(1);
    DECLARE cpu_mano_tres VARCHAR(1);
    DECLARE ply_mano_tres VARCHAR(1);
    
    SET id_game_cpu = (SELECT possible_moves.possible_move_id FROM possible_moves WHERE possible_moves.possible_move_order = cpu);
    SET id_game_ply = (SELECT possible_moves.possible_move_id FROM possible_moves WHERE possible_moves.possible_move_order = ply);
    INSERT INTO match_results (match_result_cpu_move_id, match_result_ply_move_id, match_result_win) VALUES (id_game_cpu,id_game_ply,win);
    
    SET count_j = (SELECT hands.hand_cont FROM hands WHERE hands.hand_id = 1);
    SET count_k = (SELECT hands.hand_cont FROM hands WHERE hands.hand_id = 2);
    SET count_p = (SELECT hands.hand_cont FROM hands WHERE hands.hand_id = 3);
    SET cpu_mano_uno = SUBSTRING(cpu,1,1);
    SET ply_mano_uno = SUBSTRING(ply,1,1);
    SET cpu_mano_dos = SUBSTRING(cpu,2,1);
    SET ply_mano_dos = SUBSTRING(ply,2,1);
    SET cpu_mano_tres = SUBSTRING(cpu,3,1);
    SET ply_mano_tres = SUBSTRING(ply,3,1);
        
    CASE cpu_mano_uno 
    	WHEN "J" THEN SET count_j = count_j +1;
        WHEN "K" THEN SET count_k = count_k +1;
        WHEN "P" THEN SET count_p = count_p +1;
    END CASE;
    CASE ply_mano_uno 
    	WHEN "J" THEN SET count_j = count_j +1;
        WHEN "K" THEN SET count_k = count_k +1;
        WHEN "P" THEN SET count_p = count_p +1;
    END CASE;
    CASE cpu_mano_dos 
    	WHEN "J" THEN SET count_j = count_j +1;
        WHEN "K" THEN SET count_k = count_k +1;
        WHEN "P" THEN SET count_p = count_p +1;
    END CASE;
    CASE ply_mano_dos 
    	WHEN "J" THEN SET count_j = count_j +1;
        WHEN "K" THEN SET count_k = count_k +1;
        WHEN "P" THEN SET count_p = count_p +1;
    END CASE;
    CASE cpu_mano_tres 
    	WHEN "J" THEN SET count_j = count_j +1;
        WHEN "K" THEN SET count_k = count_k +1;
        WHEN "P" THEN SET count_p = count_p +1;
    END CASE;
    CASE ply_mano_tres
    	WHEN "J" THEN SET count_j = count_j +1;
        WHEN "K" THEN SET count_k = count_k +1;
        WHEN "P" THEN SET count_p = count_p +1;
    END CASE;    
    UPDATE hands SET hands.hand_cont = count_j WHERE hands.hand_id = 1;
    UPDATE hands SET hands.hand_cont = count_k WHERE hands.hand_id = 2;
    UPDATE hands SET hands.hand_cont = count_p WHERE hands.hand_id = 3;
    	
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `best_game_cpu`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `best_game_cpu` (
`best_game` bigint(21)
,`id_move` tinyint(4)
,`move` varchar(5)
,`mano_1` varchar(1)
,`mano_1_icon` varchar(45)
,`mano_2` varchar(1)
,`mano_2_icon` varchar(45)
,`mano_3` varchar(1)
,`mano_3_icon` varchar(45)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `best_game_ply`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `best_game_ply` (
`best_game` bigint(21)
,`id_move` tinyint(4)
,`move` varchar(5)
,`mano_1` varchar(1)
,`mano_1_icon` varchar(45)
,`mano_2` varchar(1)
,`mano_2_icon` varchar(45)
,`mano_3` varchar(1)
,`mano_3_icon` varchar(45)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hands`
--

CREATE TABLE `hands` (
  `hand_id` tinyint(4) NOT NULL,
  `hand_name` varchar(10) DEFAULT NULL,
  `hand_prefix` char(1) DEFAULT NULL,
  `hand_icon` varchar(45) DEFAULT NULL,
  `hand_cont` int(10) UNSIGNED DEFAULT '0' COMMENT 'cantidad de veces que fue elegida'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Esta tabla es para las 3 manos';

--
-- Volcado de datos para la tabla `hands`
--

INSERT INTO `hands` (`hand_id`, `hand_name`, `hand_prefix`, `hand_icon`, `hand_cont`) VALUES
(1, 'piedra', 'J', 'fas fa-hand-rock', 124),
(2, 'papel', 'K', 'fas fa-hand-paper', 127),
(3, 'tijera', 'P', 'fas fa-hand-scissors', 110);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `match_results`
--

CREATE TABLE `match_results` (
  `match_result_id` int(10) UNSIGNED NOT NULL,
  `match_result_cpu_move_id` tinyint(4) DEFAULT NULL,
  `match_result_ply_move_id` tinyint(4) DEFAULT NULL,
  `match_result_win` char(1) DEFAULT NULL COMMENT 'Se toma 0 si gano CPU y 1 si gano el Jugador 2 para Empate'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Resultados de las partidas';

--
-- Volcado de datos para la tabla `match_results`
--

INSERT INTO `match_results` (`match_result_id`, `match_result_cpu_move_id`, `match_result_ply_move_id`, `match_result_win`) VALUES
(1, 5, 6, '1'),
(2, 25, 24, '0'),
(3, 27, 11, '0'),
(4, 15, 4, '2'),
(5, 7, 14, '1'),
(6, 7, 20, '1'),
(7, 2, 12, '1'),
(8, 3, 1, '1'),
(9, 11, 24, '1'),
(10, 13, 24, '2'),
(11, 14, 18, '1'),
(12, 20, 4, '1'),
(13, 14, 6, '2'),
(14, 17, 4, '0'),
(15, 25, 22, '0'),
(16, 7, 4, '0'),
(17, 6, 5, '0'),
(18, 23, 13, '0'),
(19, 21, 15, '2'),
(20, 21, 6, '1'),
(21, 9, 10, '1'),
(22, 5, 6, '1'),
(23, 13, 16, '1'),
(24, 24, 4, '1'),
(25, 22, 15, '0'),
(26, 3, 4, '1'),
(27, 18, 4, '0'),
(28, 27, 10, '1'),
(29, 22, 5, '1'),
(30, 5, 18, '1'),
(31, 24, 24, '2'),
(32, 13, 4, '0'),
(33, 6, 6, '2'),
(34, 11, 22, '1'),
(35, 15, 6, '0'),
(36, 20, 6, '1'),
(37, 18, 6, '0'),
(38, 3, 24, '2'),
(39, 12, 26, '0'),
(40, 24, 2, '0'),
(41, 13, 4, '0'),
(42, 20, 17, '0'),
(43, 10, 4, '2'),
(44, 26, 6, '1'),
(45, 4, 6, '0'),
(46, 27, 4, '1'),
(47, 4, 27, '0'),
(48, 13, 17, '1'),
(49, 3, 16, '1'),
(50, 18, 6, '0'),
(51, 15, 4, '2'),
(52, 25, 6, '0'),
(53, 16, 10, '1'),
(54, 11, 9, '0'),
(55, 20, 12, '2'),
(56, 15, 17, '2'),
(57, 6, 4, '1'),
(58, 15, 12, '0'),
(59, 12, 12, '2'),
(60, 2, 11, '1'),
(61, 18, 4, '0'),
(62, 16, 26, '1'),
(63, 11, 26, '2'),
(64, 3, 5, '2'),
(65, 6, 20, '0'),
(66, 24, 24, '2'),
(67, 20, 6, '1'),
(68, 14, 6, '2'),
(69, 17, 6, '0'),
(70, 2, 4, '2'),
(71, 24, 17, '0'),
(72, 11, 3, '2'),
(73, 22, 24, '0'),
(74, 20, 2, '1'),
(75, 21, 16, '0'),
(76, 5, 16, '1'),
(77, 9, 8, '0'),
(78, 1, 20, '2'),
(79, 12, 16, '2'),
(80, 8, 8, '2'),
(81, 20, 10, '0'),
(82, 16, 16, '2'),
(83, 17, 12, '1'),
(84, 27, 16, '2'),
(85, 26, 16, '0'),
(86, 6, 16, '1'),
(87, 16, 12, '2'),
(88, 26, 12, '1'),
(89, 16, 20, '1'),
(90, 9, 12, '1'),
(91, 14, 12, '2'),
(92, 11, 12, '1'),
(93, 2, 20, '0'),
(94, 15, 6, '0'),
(95, 9, 4, '2'),
(96, 2, 11, '1'),
(97, 17, 12, '1'),
(98, 8, 4, '0'),
(99, 19, 20, '1'),
(100, 4, 6, '0'),
(101, 8, 6, '2'),
(102, 21, 16, '0'),
(103, 14, 12, '2'),
(104, 19, 16, '0'),
(105, 18, 20, '1'),
(106, 20, 22, '2'),
(107, 8, 6, '2'),
(108, 11, 22, '1'),
(109, 20, 6, '1'),
(110, 13, 6, '0'),
(111, 22, 6, '2'),
(112, 18, 6, '0'),
(113, 4, 22, '0'),
(114, 17, 22, '0'),
(115, 3, 12, '1'),
(116, 4, 22, '0'),
(117, 24, 12, '0'),
(118, 26, 22, '0'),
(119, 26, 22, '0'),
(120, 5, 6, '1'),
(121, 13, 6, '0'),
(122, 27, 6, '2'),
(123, 14, 22, '2'),
(124, 11, 6, '1'),
(125, 2, 12, '1'),
(126, 15, 22, '1'),
(127, 16, 22, '2'),
(128, 27, 6, '2'),
(129, 11, 8, '0'),
(130, 10, 6, '0'),
(131, 4, 22, '0'),
(132, 6, 6, '2'),
(133, 14, 6, '2'),
(134, 12, 22, '1'),
(135, 27, 22, '2'),
(136, 12, 12, '2'),
(137, 21, 12, '0'),
(138, 1, 20, '2'),
(139, 18, 22, '1'),
(140, 9, 12, '1'),
(141, 14, 6, '2'),
(142, 2, 20, '0'),
(143, 17, 6, '0'),
(144, 12, 6, '2'),
(145, 5, 16, '1'),
(146, 7, 20, '1'),
(147, 20, 22, '2'),
(148, 4, 20, '0'),
(149, 24, 22, '1'),
(150, 19, 22, '1'),
(151, 5, 12, '1'),
(152, 17, 20, '1'),
(153, 16, 16, '2'),
(154, 3, 6, '1'),
(155, 8, 16, '2'),
(156, 17, 16, '0'),
(157, 18, 20, '1'),
(158, 11, 8, '0'),
(159, 27, 12, '2'),
(160, 18, 16, '1'),
(161, 3, 6, '1'),
(162, 2, 6, '1'),
(163, 17, 8, '0'),
(164, 21, 12, '0'),
(165, 16, 20, '1'),
(166, 3, 12, '1'),
(167, 7, 22, '0'),
(168, 13, 16, '1'),
(169, 16, 16, '2'),
(170, 13, 12, '0'),
(171, 17, 16, '0'),
(172, 20, 16, '0'),
(173, 25, 16, '0'),
(174, 3, 16, '1'),
(175, 10, 16, '0'),
(176, 1, 16, '2'),
(177, 17, 16, '0'),
(178, 23, 1, '0'),
(179, 8, 1, '2'),
(180, 10, 1, '0'),
(181, 11, 14, '1'),
(182, 3, 20, '0'),
(183, 10, 1, '0'),
(184, 26, 1, '1'),
(185, 23, 23, '2'),
(186, 6, 6, '2'),
(187, 21, 24, '1'),
(188, 13, 21, '0'),
(189, 11, 14, '1'),
(190, 4, 14, '1'),
(191, 15, 14, '0'),
(192, 26, 20, '1'),
(193, 12, 6, '2'),
(194, 4, 6, '0'),
(195, 6, 22, '2'),
(196, 25, 6, '0'),
(197, 19, 10, '0'),
(198, 15, 22, '1'),
(199, 15, 6, '0'),
(200, 16, 22, '2'),
(201, 5, 16, '1'),
(202, 2, 8, '0'),
(203, 25, 12, '0'),
(204, 5, 1, '0'),
(205, 13, 1, '0'),
(206, 9, 1, '1'),
(207, 13, 1, '0'),
(208, 3, 1, '1'),
(209, 1, 6, '2'),
(210, 19, 22, '1'),
(211, 1, 16, '2'),
(212, 9, 12, '1'),
(213, 6, 6, '2'),
(214, 5, 12, '1'),
(215, 10, 22, '1'),
(216, 18, 16, '1'),
(217, 24, 22, '1'),
(218, 21, 12, '0'),
(219, 24, 22, '1'),
(220, 5, 6, '1'),
(221, 5, 22, '0'),
(222, 11, 12, '1'),
(223, 17, 20, '1'),
(224, 16, 16, '2'),
(225, 3, 6, '1'),
(226, 5, 6, '1'),
(227, 3, 6, '1'),
(228, 14, 6, '2'),
(229, 8, 6, '2'),
(230, 11, 22, '1'),
(231, 11, 22, '1'),
(232, 24, 22, '1'),
(233, 27, 22, '2'),
(234, 8, 22, '0'),
(235, 21, 15, '2'),
(236, 4, 22, '0'),
(237, 2, 16, '0'),
(238, 11, 22, '1'),
(239, 12, 12, '2'),
(240, 19, 12, '0'),
(241, 9, 8, '0'),
(242, 17, 22, '0'),
(243, 18, 6, '0'),
(244, 22, 23, '1'),
(245, 22, 23, '1'),
(246, 1, 23, '1'),
(247, 27, 22, '2'),
(248, 13, 20, '1'),
(249, 2, 22, '0'),
(250, 22, 23, '1'),
(251, 13, 23, '1'),
(252, 6, 23, '0'),
(253, 5, 23, '0'),
(254, 13, 23, '1'),
(255, 4, 23, '2'),
(256, 20, 23, '1'),
(257, 17, 23, '2'),
(258, 4, 23, '2'),
(259, 15, 17, '2'),
(260, 12, 17, '0'),
(261, 26, 15, '0'),
(262, 9, 23, '0'),
(263, 6, 12, '2'),
(264, 15, 10, '2'),
(265, 20, 2, '1'),
(266, 9, 16, '1'),
(267, 24, 11, '0'),
(268, 9, 8, '0'),
(269, 17, 24, '1'),
(270, 2, 4, '2'),
(271, 5, 12, '1'),
(272, 16, 4, '0');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `number_of_games`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `number_of_games` (
`winner` bigint(21)
,`name` varchar(3)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `possible_moves`
--

CREATE TABLE `possible_moves` (
  `possible_move_id` tinyint(4) NOT NULL,
  `possible_move_order` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Todas las combinaciones posibles de jugadas';

--
-- Volcado de datos para la tabla `possible_moves`
--

INSERT INTO `possible_moves` (`possible_move_id`, `possible_move_order`) VALUES
(1, 'JJJ'),
(2, 'JJK'),
(3, 'JJP'),
(4, 'JKJ'),
(5, 'JKK'),
(6, 'JKP'),
(7, 'JPJ'),
(8, 'JPK'),
(9, 'JPP'),
(10, 'KJJ'),
(11, 'KJK'),
(12, 'KJP'),
(13, 'KKJ'),
(14, 'KKK'),
(15, 'KKP'),
(16, 'KPJ'),
(17, 'KPK'),
(18, 'KPP'),
(19, 'PJJ'),
(20, 'PJK'),
(21, 'PJP'),
(22, 'PKJ'),
(23, 'PKK'),
(24, 'PKP'),
(25, 'PPJ'),
(26, 'PPK'),
(27, 'PPP');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `total_games`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `total_games` (
`total` bigint(21)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `best_game_cpu`
--
DROP TABLE IF EXISTS `best_game_cpu`;

CREATE VIEW `best_game_cpu`  AS SELECT count(`match_results`.`match_result_cpu_move_id`) AS `best_game`, `match_results`.`match_result_cpu_move_id` AS `id_move`, (select `possible_moves`.`possible_move_order` from `possible_moves` where (`possible_moves`.`possible_move_id` = `match_results`.`match_result_cpu_move_id`)) AS `move`, (select substr(`possible_moves`.`possible_move_order`,1,1) from `possible_moves` where (`possible_moves`.`possible_move_id` = `match_results`.`match_result_cpu_move_id`)) AS `mano_1`, (select `hands`.`hand_icon` from `hands` where (`hands`.`hand_prefix` = `mano_1`)) AS `mano_1_icon`, (select substr(`possible_moves`.`possible_move_order`,2,1) from `possible_moves` where (`possible_moves`.`possible_move_id` = `match_results`.`match_result_cpu_move_id`)) AS `mano_2`, (select `hands`.`hand_icon` from `hands` where (`hands`.`hand_prefix` = `mano_2`)) AS `mano_2_icon`, (select substr(`possible_moves`.`possible_move_order`,3,1) from `possible_moves` where (`possible_moves`.`possible_move_id` = `match_results`.`match_result_cpu_move_id`)) AS `mano_3`, (select `hands`.`hand_icon` from `hands` where (`hands`.`hand_prefix` = `mano_3`)) AS `mano_3_icon` FROM `match_results` WHERE (`match_results`.`match_result_win` = 0) GROUP BY `match_results`.`match_result_cpu_move_id` ORDER BY `best_game` DESC LIMIT 0, 3 ;

-- --------------------------------------------------------

--
-- Estructura para la vista `best_game_ply`
--
DROP TABLE IF EXISTS `best_game_ply`;

CREATE VIEW `best_game_ply`  AS SELECT count(`match_results`.`match_result_ply_move_id`) AS `best_game`, `match_results`.`match_result_ply_move_id` AS `id_move`, (select `possible_moves`.`possible_move_order` from `possible_moves` where (`possible_moves`.`possible_move_id` = `match_results`.`match_result_ply_move_id`)) AS `move`, (select substr(`possible_moves`.`possible_move_order`,1,1) from `possible_moves` where (`possible_moves`.`possible_move_id` = `match_results`.`match_result_ply_move_id`)) AS `mano_1`, (select `hands`.`hand_icon` from `hands` where (`hands`.`hand_prefix` = `mano_1`)) AS `mano_1_icon`, (select substr(`possible_moves`.`possible_move_order`,2,1) from `possible_moves` where (`possible_moves`.`possible_move_id` = `match_results`.`match_result_ply_move_id`)) AS `mano_2`, (select `hands`.`hand_icon` from `hands` where (`hands`.`hand_prefix` = `mano_2`)) AS `mano_2_icon`, (select substr(`possible_moves`.`possible_move_order`,3,1) from `possible_moves` where (`possible_moves`.`possible_move_id` = `match_results`.`match_result_ply_move_id`)) AS `mano_3`, (select `hands`.`hand_icon` from `hands` where (`hands`.`hand_prefix` = `mano_3`)) AS `mano_3_icon` FROM `match_results` WHERE (`match_results`.`match_result_win` = 1) GROUP BY `match_results`.`match_result_ply_move_id` ORDER BY `best_game` DESC LIMIT 0, 3 ;

-- --------------------------------------------------------

--
-- Estructura para la vista `number_of_games`
--
DROP TABLE IF EXISTS `number_of_games`;

CREATE VIEW `number_of_games`  AS SELECT count(`match_results`.`match_result_win`) AS `winner`, if((`match_results`.`match_result_win` = 0),'cpu',if((`match_results`.`match_result_win` = 1),'ply','tie')) AS `name` FROM `match_results` GROUP BY `match_results`.`match_result_win` ;

-- --------------------------------------------------------

--
-- Estructura para la vista `total_games`
--
DROP TABLE IF EXISTS `total_games`;

CREATE VIEW `total_games`  AS SELECT count(`match_results`.`match_result_id`) AS `total` FROM `match_results` ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
