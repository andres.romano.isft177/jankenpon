## Jan Ken Pon! 

Así se denomina, en Japón, al clásico juego Piedra Papel o Tijera. El juego está escrito en HTML y CSS con algo de Bootstrap y cómo es una SPA (single page application) se utilizó el FrameWork Vue de JavaScript y AXIOS en los procesos asíncronos. El objetivo es recolectar mediciones de las partidas disputadas contra el ordenador, simplemente para practicar medición y análisis de datos.

### Configuración

#### Variables de entorno

> **SQL_SERVER** Nombre del servidor de Bases de Datos (MySQL)

> **JKP_DATABASE** El nombre de la base de datos debe ser jankenpon

> **JKP_USER** El usuario de la base de datos debe ser jankenpon y con privilegios ya que debe poder ejecutar los SP y VIEWS correspondientes

> **JKP_PASSWORD** La password pude ser la que usted desee

